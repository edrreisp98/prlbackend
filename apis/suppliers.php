<?php 

//Listar
$app->get('/suppliers',function() use($app,$conn){
	$consulta="SELECT * FROM suppliers WHERE is_deleted=0 ORDER BY id DESC;";
	try{		
	  	$statement=$conn->prepare($consulta);
	  	$statement->execute();
	  	$suppliers=$statement->fetchAll(PDO::FETCH_ASSOC);
	  	$result=array(
		    'status'=>'success',
		    'code'=>200,
		    'data'=>$suppliers
		);	
	}catch (Exception $e) {		
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	);
	}  
	echo json_encode($result);	
});

//Devolver un solo registro
$app->get('/supplier/:id',function($id) use($app,$conn){
	$consulta="SELECT * FROM suppliers WHERE id=:id";
	try{		
	  	$statement=$conn->prepare($consulta);
	  	$statement->execute(array(':id'=>$id));

	  	$result=array(
		    'status'=>'error',
		    'code'=>404,
		    'message'=>'Categoría no encontrada'
		);

	  	if($statement->rowCount()==1){
	  		$supplier=$statement->fetchAll(PDO::FETCH_ASSOC);
	  		$result=array(
	  			'status'=>'success',
			    'code'=>200,
			    'data'=>$supplier
	  		);
	  	} 
	} catch (Exception $e) {
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	); 
	}
  	echo json_encode($result);
});

//Eliminar un registro
$app->get('/delete-supplier/:id',function($id) use($app,$conn){
	$consulta="UPDATE suppliers SET is_deleted=1 WHERE id =:id";
	try {		
	  	$statement=$conn->prepare($consulta);	  	
	  	$result=array(
		    'status'=>'error',
		    'code'=>303,
			'message'=>'El suplidor no se ha eliminado'
		);
	  	if($statement->execute(array(':id'=>$id))){
	  		$result=array(
		      'status'=>'success',
		      'code'=>200,
		      'message'=>'El suplidor se ha eliminado correctamente'
		    );
	  	}
	}catch (Exception $e) {
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	);
	} 
  	echo json_encode($result);
});

//Actualizar un registro
$app->post('/update-supplier/:id',function($id) use($app,$conn){	
	$json=$app->request->post('json');
	$data=json_decode($json,true);
	
  	$consulta="UPDATE suppliers SET name=:name,phone=:phone,updated_at=:updated_at WHERE id=:id";
	try {
		$statement=$conn->prepare($consulta);
	  	$result=array(
	      'status'=>'error',
	      'code'=>404,
	      'message'=>'Suplidor no se ha actualizado correctamente'
	    );	    
	  	if($statement->execute(
	  		array(
	  			':id'=>$id,
	  			':name'=>$data['name'],
	  			':phone'=>$data['phone'],
	  			':updated_at'=>date('Y-m-d H:i:s')
	  		)
	  	)){
	  		$result=array(
		      'status'=>'success',
		      'code'=>200,
		      'message'=>'Suplidor se ha actualizado correctamente'
		    );
	  	}
	}catch (Exception $e) {
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	);
	}  
  	echo json_encode($result);  
});

//Guardar registros
$app->post('/suppliers',function() use($app,$conn){
	$json=$app->request->post('json');
	$data=json_decode($json,true);
	
  	$consulta="INSERT INTO suppliers(name,phone,created_at,updated_at) VALUES(:name,:phone,:created_at,:updated_at)";
	try {
		$statement=$conn->prepare($consulta);
	  	$result=array(
	      'status'=>'error',
	      'code'=>404,
	      'message'=>'Suplidor no se ha insertado correctamente'
	    );
	    $statement->execute(
	  		array(
	  			':name'=>$data['name'],
	  			':phone'=>$data['phone'],
	  			':created_at'=>date('Y-m-d H:i:s'),
	  			':updated_at'=>date('Y-m-d H:i:s')
	  		)
	  	);
	  	if($statement->rowCount()>0){
	  		$result=array(
		      'status'=>'success',
		      'code'=>200,
		      'message'=>'Suplidor se ha insertado correctamente'
		    );
	  	}
	}catch (Exception $e) {
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	);
	}  
  	echo json_encode($result);	
});
?>