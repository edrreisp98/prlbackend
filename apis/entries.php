<?php 
//Listar entrie
$app->get('/entries',function() use($app,$conn){
	$consultaEntries="SELECT e.*,s.name as 'supplier_name' FROM entry e 
	INNER JOIN suppliers s ON s.id=e.supplier_id
	WHERE e.is_deleted=0 ORDER BY e.id DESC;";
	try{		
	  	$statementEntry=$conn->prepare($consultaEntries);
	  	$statementEntry->execute();

	  	$entries=$statementEntry->fetchAll(PDO::FETCH_ASSOC);
	  	//Por cada ENTRY busco sus EntryDetails
	  	$index = 0;
	  	foreach($entries as $entry){
	  		$consultaEntryDetails="SELECT ed.*,p.name as 'product_name',p.sale_price,p.code as 'product_code' 
			FROM entry_detail ed INNER JOIN products p ON p.id=ed.product_id WHERE ed.entry_id=:entry_id;";
	  		$statementDetail=$conn->prepare($consultaEntryDetails);
	  		$statementDetail->execute(array(':entry_id'=>$entry["id"]));
	  		$details=$statementDetail->fetchAll(PDO::FETCH_ASSOC);
	  		$entries[$index]["entry_details"]=$details;
	  		$index++;
	  	}
	  	$result=array(
		    'status'=>'success',
		    'code'=>200,
		    'data'=>$entries
		);	
	}catch (Exception $e) {		
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	);
	}  
	echo json_encode($result);	
});
//Devolver un solo producto
$app->get('/entryanulado/:id',function($id) use($app,$conn){
	
	try{		
	  	$statement=$conn->prepare($consulta);
	  	$statement->execute(array(':id'=>$id));

	  	$result=array(
		    'status'=>'error',
		    'code'=>404,
		    'message'=>'Entry NO ANULADO'
		);

	  	if($statement->rowCount()==1){
	  		$result=array(
	  			'status'=>'success',
			    'code'=>200,
			    'data'=>'ENTRYANULADO'
	  		);
	  	} 
	} catch (Exception $e) {
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	);
	}
  	echo json_encode($result);
});

$app->post('/entry-cancel',function() use($app,$conn){
	$json=$app->request->post('json');
	$data=json_decode($json,true);
	
	$consultaComprobarSiEstaAnulado="SELECT status FROM entry WHERE id=:id AND status='ANULADO'";

  	$consultaAnular="UPDATE entry SET status='ANULADO' WHERE id=:id AND status='EMITIDO'";
  	if($data['entry_type']=="entry"){
  		$consultaUpdateInventory="UPDATE inventory SET stock=stock-:quantity WHERE product_id=:product_id";
  	}else if($data['entry_type']=="return"){
  		$consultaUpdateInventory="UPDATE inventory SET stock=stock+:quantity WHERE product_id=:product_id";
  	}
  	

	try {
		$statementComprobar=$conn->prepare($consultaComprobarSiEstaAnulado);
		$statementAnular=$conn->prepare($consultaAnular);
		$statementUpdateInventory=$conn->prepare($consultaUpdateInventory);

		$statementComprobar->execute(array(':id'=>$data['id']));

	  	if($statementComprobar->rowCount()==1){
	  		//Si entro aqui quiere decir que el entry ya fue anulado
	  		$result=array(
	  			'status'=>'success',
			    'code'=>300,
			    'data'=>'ENTRYANULADO'
	  		);
	  	}else{
	  		$result=array(
		      'status'=>'error',
		      'code'=>404,
		      'message'=>'Ingreso no anulado correctamente'
		    );	    
		  	try {
		  		$conn->beginTransaction();
		  		$statementAnular->execute(
			  		array(
			  			':id'=>$data['id']	  			
			  		)
			  	);
		  		foreach($data['entry_details'] as $entry_detail){
		    		$statementUpdateInventory->execute(
		    			array(
		    				':product_id'=>$entry_detail['product_id'],
		    				':quantity'=>$entry_detail['quantity'],
		    			)
		    		);
		    		if(!$statementUpdateInventory->rowCount()>0){
		    			throw new PDOException("Error Processing Request");	    			
		    		}
		    	}	
		  		$conn->commit();
		  		$result=array(
			      'status'=>'success',
			      'code'=>200,
			      'message'=>'Ingreso se ha anulado correctamente'
			    );
		  	} catch (PDOException $e) {
		  		$conn->rollBack();
		  		$result=array(
			  		'status'=>'error',
			  		'message'=>$e->getMessage()
			  	);
		  	}  
	  	} 	  	 
    }catch (Exception $e) {
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	);
	}  
  	echo json_encode($result);	
});

$app->post('/entries',function() use($app,$conn){
	$json=$app->request->post('json');
	$data=json_decode($json,true);	
	
  	$consultaInsertEntry="INSERT INTO entry(supplier_id,entry_type,description,datetime,total,status,comment,created_at,updated_at)
  	    VALUES(:supplier_id,:entry_type,:description,:datetime,:total,:status,:comment,:created_at,:updated_at)";

  	$consultaInsertDetail="INSERT INTO entry_detail(entry_id,product_id,quantity,cost_price) VALUES(:entry_id,:product_id,:quantity,:cost_price)";
  	if($data['entry_type']=="entry"){
  		$consultaUpdateInventory="UPDATE inventory SET stock=stock+:quantity WHERE product_id=:product_id";
  	}else if($data['entry_type']=="return"){
  		$consultaUpdateInventory="UPDATE inventory SET stock=stock-:quantity WHERE product_id=:product_id";
  	}
  	
	try {
		$result=array(
	      'status'=>'error',
	      'code'=>404,
	      'message'=>'Ingreso no ingresado'
	    );	 

		$statementEntry=$conn->prepare($consultaInsertEntry);	 
		$statementEntryDetail=$conn->prepare($consultaInsertDetail);
		$statementUpdateInventory=$conn->prepare($consultaUpdateInventory);
		try {
			$conn->beginTransaction(); 
		    $statementEntry->execute(
		  		array(
		  			':supplier_id'=>$data['supplier_id'], 			
		  			':entry_type'=>$data['entry_type'],	  			
		  			':description'=>$data['description'],  			
		  			':datetime'=>date('Y-m-d H:i:s'),	  			
		  			':total'=>$data['total'],	  			
		  			':status'=>$data['status'],	  			
		  			':comment'=>$data['comment'],	  			
		  			':created_at'=>date('Y-m-d H:i:s'),	  			
		  			':updated_at'=>date('Y-m-d H:i:s')  			
		  		)
		  	);		   

	    	$entry_id=$conn->lastInsertId();	    	

	    	foreach($data['entry_details'] as $entry_detail){
	    		$statementEntryDetail->execute(
	    			array(
	    				':entry_id'=>$entry_id,
	    				':product_id'=>$entry_detail['product_id'],
	    				':quantity'=>$entry_detail['quantity'],
	    				':cost_price'=>$entry_detail['cost_price']
	    				)
	    		);
	    		$statementUpdateInventory->execute(
	    			array(
	    				':product_id'=>$entry_detail['product_id'],
	    				':quantity'=>$entry_detail['quantity'],
	    			)
	    		);
	    		if(!$statementUpdateInventory->rowCount()>0 || !$statementEntryDetail->rowCount()>0){
	    			throw new PDOException('Could not update positions.');
	    		}	    		
	    	}	  
		  	$conn->commit(); 
		  	$result=array(
		      'status'=>'success',
		      'code'=>200,
		      'message'=>'Ingreso aprovisionado correctamente'
		    );
		} catch (PDOException $e) {
			$conn->rollback(); 
			$result=array(
		  		'status'=>'error',
		  		'message'=>$e->getMessage()
		  	);
		}			    
	}catch (Exception $e) {
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	);
	}  
  	echo json_encode($result);	
});
?>