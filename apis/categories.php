<?php 

//Listar
$app->get('/categories',function() use($app,$conn){
	$consulta="SELECT * FROM categories WHERE is_deleted=0 ORDER BY id DESC;";
	try{		
	  	$statement=$conn->prepare($consulta);
	  	$statement->execute();
	  	$categories=$statement->fetchAll(PDO::FETCH_ASSOC);
	  	$result=array(
		    'status'=>'success',
		    'code'=>200,
		    'data'=>$categories
		);	
	}catch (Exception $e) {		
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	);
	}  
	echo json_encode($result);	
});

//Devolver un solo registro
$app->get('/category/:id',function($id) use($app,$conn){
	$consulta="SELECT * FROM categories WHERE id=:id";
	try{		
	  	$statement=$conn->prepare($consulta);
	  	$statement->execute(array(':id'=>$id));

	  	$result=array(
		    'status'=>'error',
		    'code'=>404,
		    'message'=>'Categoría no encontrada'
		);

	  	if($statement->rowCount()==1){
	  		$categorie=$statement->fetchAll(PDO::FETCH_ASSOC);
	  		$result=array(
	  			'status'=>'success',
			    'code'=>200,
			    'data'=>$categorie
	  		);
	  	} 
	} catch (Exception $e) {
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	); 
	}
  	echo json_encode($result);
});

//Eliminar un registro
$app->get('/delete-category/:id',function($id) use($app,$conn){
	$consulta="UPDATE categories SET is_deleted=1 WHERE id =:id";
	try {		
	  	$statement=$conn->prepare($consulta);	  	
	  	$result=array(
		    'status'=>'error',
		    'code'=>303,
			'message'=>'La categoría no se ha eliminado'
		);
	  	if($statement->execute(array(':id'=>$id))){
	  		$result=array(
		      'status'=>'success',
		      'code'=>200,
		      'message'=>'La categoría se ha eliminado correctamente'
		    );
	  	}
	}catch (Exception $e) {
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	);
	} 
  	echo json_encode($result);
});

//Actualizar un registro
$app->post('/update-category/:id',function($id) use($app,$conn){	
	$json=$app->request->post('json');
	$data=json_decode($json,true);
	if(!isset($data['description'])){
		$data['description']=null;
  	}  
  	$consulta="UPDATE categories SET name=:name,description=:description,updated_at=:updated_at WHERE id=:id";
	try {
		$statement=$conn->prepare($consulta);
	  	$result=array(
	      'status'=>'error',
	      'code'=>404,
	      'message'=>'Categoría no se ha actualizado correctamente'
	    );	    
	  	if($statement->execute(
	  		array(
	  			':id'=>$id,
	  			':name'=>$data['name'],
	  			':description'=>$data['description'],
	  			':updated_at'=>date('Y-m-d H:i:s')
	  		)
	  	)){
	  		$result=array(
		      'status'=>'success',
		      'code'=>200,
		      'message'=>'Categoría se ha actualizado correctamente'
		    );
	  	}
	}catch (Exception $e) {
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	);
	}  
  	echo json_encode($result);  
});

//Guardar registros
$app->post('/categories',function() use($app,$conn){
	$json=$app->request->post('json');
	$data=json_decode($json,true);
	if(!isset($data['description'])){
		$data['description']=null;
  	}  
  	$consulta="INSERT INTO categories(name,description,created_at,updated_at) VALUES(:name,:description,:created_at,:updated_at)";
	try {
		$statement=$conn->prepare($consulta);
	  	$result=array(
	      'status'=>'error',
	      'code'=>404,
	      'message'=>'Categoría no se ha insertado correctamente'
	    );
	    $statement->execute(
	  		array(
	  			':name'=>$data['name'],
	  			':description'=>$data['description'],
	  			':created_at'=>date('Y-m-d H:i:s'),
	  			':updated_at'=>date('Y-m-d H:i:s')
	  		)
	  	);
	  	if($statement->rowCount()>0){
	  		$result=array(
		      'status'=>'success',
		      'code'=>200,
		      'message'=>'Categoría se ha insertado correctamente'
		    );
	  	}
	}catch (Exception $e) {
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	);
	}  
  	echo json_encode($result);	
});
?>