<?php 

//Listar productos
$app->get('/products',function() use($app,$conn){
	$consulta="SELECT p.*,c.name as 'category_name',s.name as 'supplier_name' FROM products p INNER JOIN suppliers s ON s.id=p.supplier_id INNER JOIN categories c ON c.id=p.category_id WHERE p.is_deleted=0 ORDER BY p.id DESC;";
	try{		
	  	$statement=$conn->prepare($consulta);
	  	$statement->execute();
	  	$products=$statement->fetchAll(PDO::FETCH_ASSOC);
	  	$result=array(
		    'status'=>'success',
		    'code'=>200,
		    'data'=>$products
		);	
	}catch (Exception $e) {		
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	);
	}  
	echo json_encode($result);	
});

//Devolver un solo producto
$app->get('/product/:id',function($id) use($app,$conn){
	$consulta="SELECT * FROM products WHERE id=:id";
	try{		
	  	$statement=$conn->prepare($consulta);
	  	$statement->execute(array(':id'=>$id));

	  	$result=array(
		    'status'=>'error',
		    'code'=>404,
		    'message'=>'Producto no disponible'
		);

	  	if($statement->rowCount()==1){
	  		$product=$statement->fetchAll(PDO::FETCH_ASSOC);
	  		$result=array(
	  			'status'=>'success',
			    'code'=>200,
			    'data'=>$product
	  		);
	  	} 
	} catch (Exception $e) {
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	);
	}
  	echo json_encode($result);
});

//Eliminar un producto
$app->get('/delete-product/:id',function($product_id) use($app,$conn){
	$consultaDeleteProduct="UPDATE products SET is_deleted=1 WHERE id =:id";
	$consultaDeleteInventory="UPDATE inventory SET is_deleted=1 WHERE product_id=:product_id";
	try {	
		$result=array(
		    'status'=>'error',
		    'code'=>404,
			'message'=>'El producto no se ha eliminado'
		);	
	  	$statementDeleteProduct=$conn->prepare($consultaDeleteProduct);
	  	$statementDeleteInventory=$conn->prepare($consultaDeleteInventory);

	  	try {
	  		$conn->beginTransaction();
	  		$statementDeleteProduct->execute(
	  			array(':id'=>$product_id)
	  		);
	  		$statementDeleteInventory->execute(
	  			array(':product_id'=>$product_id)
	  		);
	  		if(!$statementDeleteProduct->rowCount()>0 || !$statementDeleteInventory->rowCount()>0){
    			throw new PDOException('Could not update positions.');
    		}	

	  		$conn->commit();
	  		$result=array(
		      'status'=>'success',
		      'code'=>200,
		      'message'=>'El producto se ha eliminado correctamente'
		    );
	  	} catch (PDOException $e) {
	  		$conn->rollBack();
	  		$result=array(
		  		'status'=>'error',
		  		'message'=>$e->getMessage()
		  	);
	  	}  	
	}catch (Exception $e) {
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	);
	} 
  	echo json_encode($result);
});

//Actualizar un producto
$app->post('/update-product/:id',function($id) use($app,$conn){	
	$json=$app->request->post('json');
	$data=json_decode($json,true);
	
  	$consulta="UPDATE products 
  	SET name=:name,code=:code,
  	description=:description,
  	category_id=:category_id,
  	supplier_id=:supplier_id,
  	cost_price=:cost_price,
  	sale_price=:sale_price,
  	updated_at=:updated_at WHERE id=:id";
	try {
		$statement=$conn->prepare($consulta);
	  	$result=array(
	      'status'=>'error',
	      'code'=>404,
	      'message'=>'Producto no se ha actualizado correctamente'
	    );
	  	if($statement->execute(
	  		array(
	  			':id'=>$id,
	  			':name'=>$data['name'],
	  			':code'=>$data['code'],
	  			':description'=>$data['description'],
	  			':category_id'=>$data['category_id'],
	  			':supplier_id'=>$data['supplier_id'],
	  			':cost_price'=>$data['cost_price'],
	  			':sale_price'=>$data['sale_price'],
	  			':updated_at'=>date('Y-m-d H:i:s')
	  		)
	  	)){
	  		$result=array(
		      'status'=>'success',
		      'code'=>200,
		      'message'=>'Producto se ha actualizado correctamente'
		    );
	  	}
	}catch (Exception $e) {
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	);
	}  
  	echo json_encode($result);  
});

//Guardar productos
$app->post('/products',function() use($app,$conn){
	$json=$app->request->post('json');
	$data=json_decode($json,true);
	
  	$consultaInsertProduct="INSERT INTO products(name,code,description,category_id,supplier_id,cost_price,sale_price,created_at,updated_at) VALUES(:name,:code,:description,:category_id,:supplier_id,:cost_price,:sale_price,:created_at,:updated_at)";
  	$consultaInsertInventory="INSERT INTO inventory(product_id,created_at,updated_at) VALUES(:product_id,:created_at,:updated_at)";
	try {
		$result=array(
	      'status'=>'error',
	      'code'=>404,
	      'message'=>'Producto no se ha insertado correctamente'
	    );


		$statementInsertProduct=$conn->prepare($consultaInsertProduct);
		$statementInsertInventory=$conn->prepare($consultaInsertInventory);
	  	
	  	try {
	  		$conn->beginTransaction();
	  		$statementInsertProduct->execute(
		  		array(
		  			':name'=>$data['name'],
		  			':code'=>$data['code'],
		  			':description'=>$data['description'],
		  			':category_id'=>$data['category_id'],
		  			':supplier_id'=>$data['supplier_id'],
		  			':cost_price'=>$data['cost_price'],
		  			':sale_price'=>$data['sale_price'],
		  			':created_at'=>date('Y-m-d H:i:s'),
		  			':updated_at'=>date('Y-m-d H:i:s')
		  		)
		  	);	
		  	$statementInsertInventory->execute(
		  		array(
		  			':product_id'=>$conn->lastInsertId(),
		  			':created_at'=>date('Y-m-d H:i:s'),
		  			':updated_at'=>date('Y-m-d H:i:s')
		  		)
		  	);
		  	if(!$statementInsertProduct->rowCount()>0 || !$statementInsertInventory->rowCount()>0){
    			throw new PDOException('Could not update positions.');
    		}
	  		$conn->commit(); 
	  		$result=array(
		      'status'=>'success',
		      'code'=>200,
		      'message'=>'Producto se ha insertado correctamente'
		    );
	  	} catch (PDOException $e) {
	  		$conn->rollback(); 
        	$result=array(
		  		'status'=>'error',
		  		'message'=>$e->getMessage()
		  	);
	  	}
	    
  	}catch (Exception $e) {
	  	$result=array(
	  		'status'=>'error',
	  		'message'=>$e->getMessage()
	  	);
	}  
  	echo json_encode($result);	
});
?>