<?php
date_default_timezone_set('America/Santo_Domingo');
require_once('vendor/autoload.php');
require_once('conexion/conexion.php');

$app=new \Slim\Slim();

//Instancia de conexion
$Conexion=new Conexion();
$conn=$Conexion->conectar();

//Configuracion de cabeceras
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
    die();
}
require_once('apis/products.php');
require_once('apis/categories.php');
require_once('apis/suppliers.php');
require_once('apis/entries.php');
$app->run();
